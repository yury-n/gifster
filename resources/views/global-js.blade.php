<script>
    window.gifs = {};

    window.GIF_TECH_UPLOADED = 1;
    window.GIF_TECH_URL = 2;
    window.GIF_TECH_URL_VIDEO = 3;
    window.GIF_TECH_EMPTY = 4;

    window.GIFS_PER_PAGE = {!! GIFS_PER_PAGE !!};

    window.userid = {{!Auth::guest() ? Auth::user()->id : 0}};
    window.isMobile = !!{{intval(\App\Helpers\Common::is_mobile())}};
</script>