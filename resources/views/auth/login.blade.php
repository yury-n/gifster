@extends('layout')

@section('title')Вход@stop

@section('content')
    <div class="entrance">
        <div class="well bs-component">
            <form id="login-form" class="form-horizontal" method="POST" action="{{ url('/login') }}">
                <h3 class="col-lg-offset-3">Вход</h3>
                <fieldset>
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('email') || $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">E-mail</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}" maxlength="255">
                            @if ($errors->has('email') || $errors->has('password'))
                                <span class="help-block">
                                    <strong>Неверные данные авторизации</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') || $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Пароль</label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <a class="reset-link" href="{{ url('/password/reset') }}">Восстановить пароль</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <button type="submit" class="btn btn-primary">Войти</button>
                            <a href="{{ url('/register') }}" role="button" class="btn btn-default">Регистрация</a>
                        </div>
                    </div>
                    <input type="hidden" name="remember" value="1">
                </fieldset>
            </form>
        </div>
    </div>
@stop