@extends('layout')

@section('title')Регистрация@stop

@section('content')
    <div class="entrance">
        <div class="well bs-component">
            <form id="signup-form" class="form-horizontal" method="POST" action="{{ url('/register') }}">
                <h3 class="col-lg-offset-3">Регистрация</h3>
                <fieldset>
                    {!! csrf_field() !!}
                    <input type="hidden" name="origin" value="{{$origin}}">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Имя</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" maxlength="30">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">E-mail</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}" maxlength="255">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Пароль</label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                            <a href="{{ url('/login') }}" role="button" class="btn btn-default">Вход</a>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop