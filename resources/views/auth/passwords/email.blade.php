@extends('layout')

@section('title')Восстановить пароль@stop

@section('content')
    <div class="entrance">
        <div class="well bs-component">
            @if (session('status'))
                <div class="alert alert-success">
                    Мы отправили Вам письмо!
                </div>
            @endif
            <form id="login-form" class="form-horizontal" method="POST" action="{{ url('/password/email') }}">
                <h3 class="col-lg-offset-3">Восстановить пароль</h3>
                <fieldset>
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">E-mail</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}" maxlength="255">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <button type="submit" class="btn btn-primary">Прислать пароль</button>
                        </div>
                    </div>
                    <input type="hidden" name="remember" value="1">
                </fieldset>
            </form>
        </div>
    </div>
@stop