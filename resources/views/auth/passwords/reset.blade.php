@extends('layout')

@section('title')Изменить пароль@stop

@section('content')
    <div class="entrance">
        <div class="well bs-component">
            <form id="login-form" class="form-horizontal" method="POST" action="{{ url('/password/reset') }}">
                <h3 class="col-lg-offset-3">Изменить пароль</h3>
                <fieldset>
                    {!! csrf_field() !!}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">E-mail</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="email" value="{{ $email or old('email') }}" maxlength="255">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Пароль</label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Повторите пароль </label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <button type="submit" class="btn btn-primary">Сохранить пароль</button>
                        </div>
                    </div>
                    <input type="hidden" name="remember" value="1">
                </fieldset>
            </form>
        </div>
    </div>
@stop