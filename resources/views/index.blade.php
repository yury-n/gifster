@extends('layout')

@section('title')GIFSTER @stop
@section('description')GIFSTER - Поисковик гифок. Свежие гифки на разные темы. Поиск гиф. Поиск джиф. Поиск GIF-файлов.@stop

@section('content')
    @include('search-block')
    @include('results')
@stop