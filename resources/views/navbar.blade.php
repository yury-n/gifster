<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a title="гифки" id="main-logo" class="navbar-brand" href="/"><img alt="гифки" src="{{ url('/imgs/gifster_md.png') }}" /></a>
        </div>

        <a class="all-nav-buttons-opener" href="javascript:void(0);"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>

        <ul class="nav navbar-nav">
            <li class="dropdown main-menu-button">
                <a href="javascript:void(0);"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void(0)"><input id="show_mature" type="checkbox"> <label for="show_mature">Показывать контент 16+</label></a></li>
                    <!--<li><a href="javascript:void(0)"><input id="play_on_hover" type="checkbox"> <label for="play_on_hover">Запускать при наведении</label></a></li>-->
                    <li><a href="/search/">Поиск по тэгам</a></li>
                    <li><a id="leave-feedback" href="javascript:void(0)">Оставить отзыв</a></li>
                </ul>
            </li>
            <li class="{{ isset($navbar_section) && $navbar_section == 'index' ? 'active' : ''}}"><a href="http://gifster.ru/"><span class="glyphicon glyphicon glyphicon glyphicon-fire" aria-hidden="true"></span> Популярное</a></li>
            <li class="{{ isset($navbar_section) && $navbar_section == 'new' ? 'active' : ''}}"><a href="{{url('new')}}"><span class="glyphicon glyphicon-leaf" aria-hidden="true"></span> Свежее</a></li>
            <!--<li class="{{ isset($navbar_section) && $navbar_section == 'categories' ? 'active' : ''}}"><a href="/categories"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> Категории</a></li>-->
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="{{ isset($navbar_section) && $navbar_section == 'faves' ? 'active' : ''}}"><a href="{{url('/faves')}}"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Закладки</a></li>
            <li class="{{ isset($navbar_section) && $navbar_section == 'gif/create' ? 'active' : ''}}"><a href="{{url('/gif/create')}}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить</a></li>
            @if (Auth::guest())
                <li class="{{ isset($navbar_section) && in_array($navbar_section, ['login', 'signup']) ? 'active' : ''}}"><a href="{{url('/login')}}"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Войти</a></li>
            @else
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Выйти</a></li>
                    </ul>
                </li>
            @endif
        </ul>
        @if (false && (!isset($navbar_section) || !in_array($navbar_section, ['index', 'search'])))
            <form class="navbar-form navbar-left navbar-search-form" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Поиск">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </div>
            </form>
        @endif

    </div>
</nav>