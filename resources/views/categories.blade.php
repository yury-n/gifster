@extends('layout')

@section('title')категории@stop

@section('content')
    <div class="container">

        @foreach ($subcategories as $subcategory)
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                <a class="thumbnail" href="{{$subcategory['url']}}">
                    <span class="img" style="background-image:url({{$subcategory['thumb']}})">
                        <span class="cat-title"><span>{{$subcategory['name']}}</span></span>
                    </span>
                </a>
            </div>
        @endforeach

    </div>
@stop