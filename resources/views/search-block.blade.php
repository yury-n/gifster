<div id="big-search-container" class="container search-container">
    <div class="form-group">
        <input id="big-search" value="{{$query or ''}}" type="text" class="form-control"><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
    </div>
</div>