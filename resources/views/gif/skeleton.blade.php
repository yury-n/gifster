<div class="gif" data-gifid="{{$id or ''}}">
    <a title="Поставить плюс" href="javascript:void(0);" class="btn btn-default btn-floating btn-upvote {{ isset($vote) && $vote > 0 ? 'active' : '' }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
    <a title="Поставить минус" href="javascript:void(0);" class="btn btn-default btn-floating btn-downvote {{ isset($vote) && $vote < 0 ? 'active' : '' }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
    <a title="В закладки" href="javascript:void(0);" class="btn btn-default btn-floating btn-fave {{ isset($is_faved) && $is_faved ? 'active' : '' }}"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span></a>
    <a title="Закрыть" href="javascript:void(0);" class="btn btn-default btn-floating btn-close"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
    <div class="gif-post">
        <div class="gif-body" itemscope="" itemtype="http://schema.org/VideoObject">
            @if ($tech_type != GIF_TECH_EMPTY)
                <meta itemprop="url" content="{{URL::current()}}">
                <meta itemprop="contentUrl" content="{{$original_gif}}">
                <meta itemprop="width" content="{{$width}}">
                <meta itemprop="height" content="{{$height}}">
                <meta itemprop="name" content="Анимированная гифка">
                <meta itemprop="dateCreated" content="{{date('Y-m-d', strtotime($created_at))}}">
                <meta itemprop="representativeOfPage" content="True">
                <meta itemprop="keywords" content="гиф, анимированная гифка, {{$shuffled_tags_str or ''}}">

                @include('next-btn')
            @endif
            <div class="gif-protector"></div>
            <div class="gif-content"></div>
        </div>
        @if ($tech_type != GIF_TECH_EMPTY)
            @if (isset($title) && $title != '')
                <h1>{{$title}}</h1>
            @else
                <h1 class="less-important">гифка {{$tags[0]['name']}}</h1>
            @endif
        @else
            <h1 style="display:none"></h1>
        @endif
        <div class="tags-and-info">
            <span class="tags">
                @if (isset($tags))
                    @foreach ($tags as $tag)<h3 class="tag"><a title="поиск по тегу &quot;{{$tag['name']}}&quot;" href="{{url('/tag/' . $tag['name'] . '/')}}"><span class="hashtag">#</span>{{$tag['name']}}</a></h3>@endforeach
                @endif
            </span>
            <span class="from-user" title="От пользователя {{$username or ''}}"
            @if(empty($userid))
                style="display:none"
            @endif>
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <span class="username">{{$username or ''}}</span>
            </span>
            <span class="score" title="Рейтинг">
                <span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
                <span class="score-value">
                @if (isset($score))
                    {{$score > 0 ? '+' . $score : $score}}
                @endif
                </span>
            </span>
        </div>
        <!--<div class="btn-share-container">
            <button type="button" class="btn btn-default btn-comments">Комментрировать</button>
            <button type="button" class="btn btn-default btn-primary btn-share">Поделиться</button>
        </div>
        -->
        <div class="share-area">
            <div class="share-btns">
            </div>
            <!--<div class="share-chat">
                <label for="inputURL" class="control-label">Для вставки в чат:</label>
                <input type="text" class="form-control share-chat-input" value="http://i.imgur.com/j1ZgDrL.webm">
            </div>-->
        </div>
    </div>
</div>