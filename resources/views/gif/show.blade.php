@extends('layout')

@section('title'){{isset($gif['title']) && $gif['title'] != '' ? $gif['title'] : implode(' ', array_column(array_slice($gif['tags'], 0, 3), 'name'))}} гиф@stop
@section('description')Открой для себя и поделись этой гифкой про {{$gif['random_tag']}}. Gifster - лучший поисковик гифок.@stop
@section('keywords'){{$gif['shuffled_tags_str']}}, гиф, анимированные гифки@stop

@section('ogmeta')
    <meta property="og:image" content="{{$gif['thumb_url']}}" />
    <meta property="og:image:width" content="290" />
    <meta property="og:image:height" content="200" />
    <meta property="og:title" content="{{$gif['title'] or ''}} гиф" />
    <meta property="og:description" content="гиф {{implode(' ', array_column(array_slice($gif['tags'], 0, 3), 'name'))}}" />
    <meta property="og:url" content="{{URL::current()}}" />
    <meta property="og:site_name" content="gifster.ru" />
    <meta property="og:type" content="website" />
@stop

@section('content')
    <script>
        $.extend(window.gifs, {!! '{' . $gif['id'] . ':' . json_encode($gif) . '}' !!});
    </script>
    <div class="container jumbotron-container">
        <div class="jumbotron">
            <div class="jumbotron-content">
                @include('gif.skeleton', $gif)
            </div>
        </div>
    </div>
    @include('results')
@stop