@extends('layout')

@section('title')Добавить GIF @stop

@section('content')
    <div class="gif-create">
        <div class="well bs-component">
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ url('/gif/store') }}">
                <h3 class="col-lg-offset-3">Добавить GIF</h3>
                <fieldset>
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('url_or_filename') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">GIF</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file">
                                        Выбрать файл&hellip; <input type="file" name="gif-file">
                                    </span>
                                </span>
                                <input type="text" class="form-control" placeholder="или по URL" name="url_or_filename" value="{{old('url_or_filename')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Теги</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="tags" placeholder="(от 2 до 10 через запятую)" maxlength="256" value="{{old('tags')}}">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-lg-3 control-label">Заголовок</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="title" placeholder="(не обязательно)" maxlength="256" value="{{old('title')}}">
                        </div>
                    </div>
                    <div class="form-group form-group-mature">
                        <div class="col-lg-9 col-lg-offset-3">
                            <input type="checkbox" id="is_mature" name="is_mature" {!! (old('is_mature') ? 'checked' : '') !!} />
                            <label for="is_mature" class="control-label">Cодержит материалы 18+</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@stop