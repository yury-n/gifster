<div id="feedback-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="btn btn-default btn-floating btn-close"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            <div class="modal-header">
                <h3 class="col-lg-offset-2">Отзыв</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <fieldset>

                        <div id="email-form-group" class="form-group">
                            <label for="feedback-email" class="col-lg-2 control-label">E-mail</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="feedback-email" value="{{!Auth::guest() ? Auth::user()->email : ''}}">
                                <p class="help-block">Заполните, пожалуйста</p>
                            </div>
                        </div>

                        <div id="msg-form-group" class="form-group">
                            <label for="feedback-text" class="col-lg-2 control-label">Отзыв</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="5" id="feedback-text"></textarea>
                                <p class="help-block">Заполните, пожалуйста</p>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-lg-offset-2">
                    <button type="button" class="btn btn-primary">Отправить</button>
                    <button type="button" class="btn btn-default btn-cancel">Отмена</button>
                </div>
            </div>
        </div>
    </div>
</div>