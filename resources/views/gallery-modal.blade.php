<div id="gallery-modal" class="modal">
    <a href="javascript:void(0);" class="btn btn-default btn-floating btn-prev"><span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
    @include('next-btn')
    <div class="modal-dialog">
        <div class="modal-content">
            @include('gif.skeleton', ['tech_type' => GIF_TECH_EMPTY])
        </div>
    </div>
</div>
<div id="url-tooltip">
    <div class="form-group">
        <input type="text" class="form-control" name="original-gif-url" value="">
    </div>
</div>