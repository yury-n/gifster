@extends('layout')

@section('title'){{$tag}}@stop

@section('content')
    @include('search-block', ['query' => $tag])
    @include('results')
@stop