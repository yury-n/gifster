<div class="container results-container">

    <div class="row">

        @if (isset($gifs) && !empty($gifs))
            <script>
                $.extend(window.gifs, {!! json_encode($gifs) !!});
            </script>
            @foreach ($gifs as $gif)
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="{{url('/gif/' . $gif['translit'] . '_' . $gif['id'])}}" data-gifid="{{$gif['id']}}">
                        <span alt="гифка {{$gif['shuffled_tags_str']}}" class="img" style="background-image:url({{$gif['thumb_url']}})">
                            @if (isset($gif['title']) && !empty($gif['title']))
                                <span class="header">{{$gif['title']}}</span>
                            @endif
                            <span class="loading"></span>
                            <span class="tags">
                                @foreach ($gif['tags'] as $tag)
                                    <span><span class="hashtag">#</span>{{$tag['name']}}</span>
                                @endforeach
                            </span>
                        </span>
                    </a>
                </div>
            @endforeach
        @endif
    </div>

</div>

@if (isset($has_more) && $has_more)
    <div class="container">
        <a href="{{(isset($next_offset_homepage) ? '/' : URL::current()) . '?' . http_build_query(['offset' => $next_offset])}}" class="btn btn-default btn-lg btn-block btn-loadmore">Загрузить еще...</a>
    </div>
@endif