<!DOCTYPE html>
<html>
<head lang="ru">

    <meta charset="UTF-8">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="ru">

    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="GIFSTER">
    <meta name="keywords" content="Гиф, гифки, поиск гифок, GIF, Animated GIF">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="shortcut icon" href="{{ url('/favicon.ico') }}">

    <link rel="stylesheet" href="{{elixir('css/app.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


    @yield('header')

    @yield('ogmeta')

</head>

<body class="gifster @if(\App\Helpers\Common::is_mobile())
        gifster-mobile
        @endif">

    @include('counters')

    @include('global-js')

    @include('navbar')

    @yield('content')

    @include('feedback-modal')
    @include('gallery-modal')

    <script src="{{elixir('js/app.js')}}"></script>

    @yield('footer')

    <iframe id="stats-iframe" src=""></iframe>

</body>

</html>