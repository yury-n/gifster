<a href="/admin_area/video_thumb_create?url={{$url}}">Go to create page</a>
<br/>
<br/>
<canvas id=c></canvas>
<video id="video" src="{{$url}}"  onerror="failed(event)" controls="controls" style="position:absolute;top:-1000000px;"></video>

<script>
    var video = document.getElementById('video');

    video.addEventListener('canplay', function() {
        this.currentTime = this.duration / 2;
    }, false);
    video.addEventListener('seeked', function() {
        getThumb();
    }, false);

    video.load();

    function getThumb(){
        var w = video.videoWidth;
        var h = video.videoHeight;
        var canvas = document.getElementById('c');
        canvas.width = w;
        canvas.height = h;
        var ctx = canvas.getContext('2d');
        ctx.drawImage(video, 0, 0, w, h);
    }

</script>