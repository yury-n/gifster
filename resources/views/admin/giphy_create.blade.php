@extends('admin.layout')

@section('content')
    <style>
        .admin-create-tags button {
            background: #F57272;
            color: #FFF;
            font-weight: bold;
            border: none;
            padding: 3px 11px;
            padding-bottom: 2px;
        }
    </style>
    <form action="{{url('/admin_area/giphy_store')}}" method="POST">
        <div class="container">
            <img src="{{$gif}}" style="float:left; margin-right:20px" />
            {!! csrf_field() !!}
            <input type="hidden" name="giphyid" value="{{$giphyid}}" />
            <input type="hidden" name="mp4" value="{{$mp4}}" />
            <input type="hidden" name="gif" value="{{$gif}}" />
            <input type="hidden" name="gif_still" value="{{$gif_still}}" />
        </div>
        <br/>
        <br/>
        <div class="container">
            <input type="text" name="score" value="1" />
        </div>
        <div class="container">
            <input type="text" name="title" placeholder="title" size="50" />
        </div>
        <br/>
        <br/>
        <div class="container admin-create-tags">
            @foreach($tags as $tag)
                <input type="text" name="tag[]" value="{{$tag}}" size="40" /><button class="input-remover">x</button><br/>
            @endforeach
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
                <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
        </div>
        <br/>
        <br/>
        <div class="container">
            <input type="submit" />
        </div>
    </form>
    <script>
        $('.admin-create-tags button').click(function(){
            $(this).prev().remove();
            $(this).next().remove();
            $(this).remove();
            return false;
        });
    </script>
@stop