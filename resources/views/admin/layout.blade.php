<!DOCTYPE html>
<html>
<head lang="en">

    <meta charset="UTF-8">

    <title>GIFSTER | ADMIN</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="shortcut icon" href="{{ url('/admin.favicon.ico') }}">

    <link rel="stylesheet" href="{{elixir('css/app.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>


</head>

<body>

@include('admin.navbar')

@yield('content')


</body>

</html>