<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/"><img style="-webkit-filter: hue-rotate(530deg);" src="{{ url('/imgs/gifster.png') }}" /></a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
                <li class=""><a href="{{url('/login')}}"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Войти</a></li>
            @else
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Выйти</a></li>
                    </ul>
                </li>
            @endif
        </ul>

    </div>
</nav>