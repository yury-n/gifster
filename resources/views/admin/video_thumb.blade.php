@extends('admin.layout')

@section('content')
    <style>
        .admin-create-tags button {
            background: #F57272;
            color: #FFF;
            font-weight: bold;
            border: none;
            padding: 3px 11px;
            padding-bottom: 2px;
        }
    </style>
    <form action="{{url('/admin_area/video_thumb_store')}}" method="POST" enctype="multipart/form-data">
        <div class="container">
            {!! csrf_field() !!}
            <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-primary btn-file">
                        Выбрать файл&hellip; <input type="file" name="gif-file">
                    </span>
                </span>
            </div>
            <input type="hidden" name="mp4" value="{{$url}}" />
        </div>
        <br/>
        <br/>
        <div class="container">
            <input type="text" name="score" value="1" />
        </div>
        <div class="container">
            <input type="text" name="title" placeholder="title" size="50" />
        </div>
        <br/>
        <br/>
        <div class="container admin-create-tags">
            @foreach($tags as $tag)
                <input type="text" name="tag[]" value="{{$tag}}" size="40" /><button class="input-remover">x</button><br/>
            @endforeach
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
            <input type="text" name="tag[]" value="" size="40" /><button>x</button><br/>
        </div>
        <br/>
        <br/>
        <div class="container">
            <input type="checkbox" name="is_mature" id="is_mature"> <label for="is_mature">is mature</label>
        </div>
        <br/>
        <br/>
        <div class="container">
            <input type="submit" />
        </div>
    </form>
    <script>
        $('.admin-create-tags button').click(function(){
            $(this).prev().remove();
            $(this).next().remove();
            $(this).remove();
            return false;
        });
    </script>
@stop