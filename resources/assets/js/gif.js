$(function(){

    $(document).on('contextmenu', '.gif-protector', function(e){

        var gifid = $(this).closest('.gif').data('gifid');

        var top = e.pageY;
        var left = e.pageX;
        $('#url-tooltip').css({'top': top, 'left': left}).show();
        var directUrl = window.gifs[gifid]['original_gif'] ? window.gifs[gifid]['original_gif'] : window.gifs[gifid]['original_mp4'];
        $('#url-tooltip input').val(directUrl).focus().select();

        $('#stats-iframe')[0].src = 'http://gifster.ru/stats/rightclickgif.html?s=' + Math.round(Math.random() * 1000);

        return false;
    });

    $(document).on('blur', '#url-tooltip input', function(e){
        $('#url-tooltip').hide();
    });

    $(document).on('click', '.pluso-wrap a', function(){
        $('#stats-iframe')[0].src = 'http://gifster.ru/stats/share.html?s=' + Math.round(Math.random() * 1000);
    });

    $(document).on('click', '.gif-body .btn-next-container', function(){
        $(this).hide();
        $('.results-container .thumbnail:first').click();
    });

    $(document).on('click', '.btn-fave', function(){

        if (!window.userid) {
            _redirect_to_signup();
            return;
        }

        var $this = $(this);
        var $gif = $this.closest('.gif');
        var gifid = $gif.data('gifid');

        if (!$this.hasClass('active')) {
            $this.addClass('active');
            $.ajax({
                url: '/gif/fave',
                method: 'POST',
                data: {
                    'gifid': gifid
                },
                success: function(){ }
            });
            window.gifs[gifid]['is_faved'] = true;
            var $upvoteBtn = $gif.find('.btn-upvote');
            $upvoteBtn.click();
        } else {
            $this.removeClass('active');
            $.ajax({
                url: '/gif/unfave',
                method: 'POST',
                data: {
                    'gifid': gifid
                },
                success: function(){ }
            });
            window.gifs[gifid]['is_faved'] = false;
        }
    });

    function _redirect_to_signup() {
        window.location = 'http://' + window.location.hostname + '/register/?origin=' + location.href;
    }

    $(document).on('click', '.btn-upvote, .btn-downvote', _update_vote);

    function _update_vote() {

        if (!window.userid) {
            _redirect_to_signup();
            return;
        }

        var $this = $(this);
        var $gif = $this.closest('.gif');
        var gifid = $gif.data('gifid');

        var type = $this.hasClass('btn-upvote') ? 'upvote' : 'downvote';

        if (!$this.hasClass('active')) {
            $this.addClass('active');
            var $opposite_btn = $gif.find(type == 'upvote'? '.btn-downvote' : '.btn-upvote');
            var opposite_vote_was_active = false;
            if ($opposite_btn.hasClass('active')) {
                opposite_vote_was_active = true;
                $opposite_btn.removeClass('active');
            }
            $.ajax({
                url: '/gif/' + type,
                method: 'POST',
                data: {
                    'gifid': gifid
                },
                success: function(){ }
            });

            var $score = $gif.find('.score');
            var $scoreValue = $gif.find('.score-value');
            var scoreValue = parseInt($scoreValue.text(), 10);
            if (type == 'upvote') {
                var newScoreValue = opposite_vote_was_active ? (scoreValue + 2) : (scoreValue + 1);
            } else {
                var newScoreValue = opposite_vote_was_active ? (scoreValue - 2) : (scoreValue - 1);
            }
            $scoreValue.text(newScoreValue > 0 ? '+' + newScoreValue : newScoreValue);
            $score.addClass('highlighted');

            window.gifs[gifid]['score'] = newScoreValue;
            window.gifs[gifid]['vote'] = (type == 'upvote' ? 1 : -1);
        }
    }

    renderShareButtons($('.share-btns'));

    $(document).on('click', '.btn-share', function(){
        $(this).addClass('active');
        showShareButtons();
    });

    function showShareButtons() {
        var inGalleryModal = $('#gallery-modal').is(':visible')
        var $shareArea = inGalleryModal ? $('#gallery-modal .share-area') : $('.share-area');
        $shareArea.addClass('open').focus();
        if (inGalleryModal) {
            if (!isScrolledIntoView($shareArea[0])) {
                $('#gallery-modal').animate({scrollTop: $shareArea.offset().top}, 'slow');
            }
        }
    }

    function isScrolledIntoView(elem) {
        var $elem = $(elem);
        var $window = $(window);

        var docViewTop = $window.scrollTop();
        var docViewBottom = docViewTop + $window.height();

        var elemTop = $elem.offset().top;
        var elemBottom = elemTop + $elem.height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

});

function updateGifPostWidth($gifPost, gifid) {

    var gifData = window.gifs[gifid];
    $gifPost.width(gifData['width']);
}

function renderGifContents($container, gifid) {

    var gifData = window.gifs[gifid];

    if (window.isMobile) {
        var padding = 10;
        if (gifData['width'] + (padding * 2) > $(window).width()) {
            var oldWidth = gifData['width'];
            var oldHeight = gifData['height'];
            var ratio = oldWidth / oldHeight;

            var newWidth = $(window).width() - (padding * 2);
            var newHeight = Math.round(newWidth / ratio);
            window.gifs[gifid]['width'] = newWidth;
            window.gifs[gifid]['height'] = newHeight;
            gifData = window.gifs[gifid];
        }
    }
    var $elem = $(gifData['content']).attr('width', gifData['width']).attr('height', gifData['height']);
    $container.html(htmlString($elem[0].outerHTML, {'width': gifData['width'].toString(), 'height': gifData['height'].toString()}));
}

function renderShareButtons($sharebtns) {
    delete window.pluso;
    delete window.ifpluso;
    $sharebtns.html('<script type="text/javascript">(function() { if (window.ifpluso==undefined) { window.ifpluso = 1;            var d = document, s = d.createElement(\'script\'), g = \'getElementsByTagName\';            s.type = \'text/javascript\'; s.charset=\'UTF-8\'; s.async = true;            s.src = \'http://share.pluso.ru/pluso-like.js\';            var h=d[g](\'body\')[0];            h.appendChild(s);        }})();</script> <div class="pluso" data-background="transparent" data-options="big,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>');
}