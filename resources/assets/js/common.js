
$(function(){

    $(document).on('keypress', 'input, textarea', function(){
        $(this).closest('.form-group').removeClass('has-error');
    });

    $(document).on('click', '.dropdown', function(){
        $(this).addClass('active open');
    });
    $(document).on('click', function(e) {
        if (!$(e.target).closest('.dropdown.open').length) {
            $('.dropdown.open').removeClass('active open');
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.all-nav-buttons-opener').click(function(){
        $('.navbar-nav').toggle();
    });

    $('#play_on_hover').on('change', function(){
        $('#play_on_hover').attr('checked', false);
        alert('Извините, пока что в разработке.');
    });

});


