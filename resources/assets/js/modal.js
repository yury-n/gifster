$(function(){

    $(document).on('click', '.modal', function(e){
        if (e.target == $('.modal:visible')[0]) {
            hide_modal();
        }
    });

    $(document).on('click', '.modal .btn-close', hide_modal);
    $(document).on('click', '.modal .btn-cancel', hide_modal);

    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // esc
            hide_modal();
        }
    });

    function hide_modal() {
        var $modal = $('.modal:visible');
        $modal[0].scrollTop = 0;
        $modal.hide();
    }

});