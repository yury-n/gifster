

/**
 * Converts object to html attributes string.
 *
 * @private
 * @param {object} obj
 * @return {string}
 */
function objectToHtmlAttributes(obj) {
    var attributes = [],
        value;
    for (var name in obj) {
        value = obj[name];
        attributes.push(name + '="' + escapeDoubleQuotes(value) + '"');
    }
    return attributes.join(' ');
}

/**
 * Given a html string returns an html node.
 *
 * @private
 * @param {string} html
 * @return {Node}
 */
function htmlToNode(html) {
    var container = document.createElement('div');
    container.innerHTML = html;
    return container.firstChild;
}

/**
 * Escapes double quotes in a string.
 *
 * @private
 * @param {string} str
 * @return {string}
 */
function escapeDoubleQuotes(str) {
    return str.split('"').join('\\"');
}

function htmlString(html, iframeAttributes) {
    var iframeAttributes  = iframeAttributes || {},
        defaultStyles = 'border:none; overflow:hidden; ',
        id;
    /*-- Setting default styles and letting the option to add more or overwrite them --*/
    if ('style' in iframeAttributes) {
        iframeAttributes.style =  defaultStyles + iframeAttributes.style;
    } else {
        iframeAttributes.style = defaultStyles;
    }
    id = '__referrer_killer_' + (new Date).getTime() + Math.floor(Math.random()*9999);
    /*-- Returning html with the hack wrapper --*/
    return '<iframe \
				scrolling="no" \
				frameborder="no" \
				allowtransparency="true" ' +
            /*-- Adding style attribute --*/
        objectToHtmlAttributes( iframeAttributes ) +
        'id="' + id + '" ' +
        '	src="javascript:\'\
        <!doctype html>\
        <html>\
        <head>\
        <meta charset=\\\'utf-8\\\'>\
        <style>*{margin:0;padding:0;border:0;}</style>\
        </head>' +
        '<body>\' + decodeURIComponent(\'' +
            /*-- Content --*/
        encodeURIComponent(html) +
        '\') +\'</body></html>\'"></iframe>';
}