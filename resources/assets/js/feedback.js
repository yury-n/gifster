
$(function(){
    $('#leave-feedback').click(function(){
        $('.modal:visible').hide();
        var $modal = $('#feedback-modal').show();
        setTimeout(function() {
            $modal.addClass('in');
            $('.dropdown.open').removeClass('active open');
        }, 100);
    });

    $('#feedback-modal .btn-primary').click(function(){
        var hasErrors = false;
        if ($('#email-form-group input').val() == '') {
            hasErrors = true;
            $('#email-form-group').addClass('has-error');
        }
        if ($('#msg-form-group textarea').val() == '') {
            hasErrors = true;
            $('#msg-form-group').addClass('has-error');
        }
        if (hasErrors) {
            return;
        }
        $(this).text('Отправляем');
        $('#feedback-modal .btn').addClass('disabled');
        $('#feedback-modal textarea, #feedback-modal input').attr('disabled', 'disabled');

        $.ajax({
            url: '/feedback/store',
            method: 'POST',
            data: {
                'email': $('#email-form-group input').val(),
                'msg': $('#msg-form-group textarea').val()
            },
            success: function(data){
                $('#feedback-modal .modal-body').html('');
                $('#feedback-modal .modal-header h3').text('Спасибо!');
                $('#feedback-modal .modal-footer').html('<div class="col-lg-offset-2" style="text-align:left;"><button type="button" class="btn btn-default btn-cancel">Закрыть</button></div>');
            }
        });
    });

});