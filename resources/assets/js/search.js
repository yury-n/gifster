$(function(){

    var placeholder = 'Поиск GIF по тэгам';

    var justAddedTag = true;

    $('#big-search').tagit({
        autocomplete: {
            delay: 0,
            minLength: 1,
            source: function(request, response) {
                $.ajax({
                    url: 'http://' + window.location.hostname + '/search_tags',
                    dataType: "json",
                    data: {
                        query: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            }
        },
        allowSpaces: true,
        tagLimit: 4,
        placeholderText: placeholder,
        afterTagAdded: function() {
            $('#big-search-container .tagit input').attr('placeholder', '');
            $('#big-search-container button').addClass('ready-to-search');
            justAddedTag = true;
            setTimeout(function(){
                justAddedTag = false;
            }, 300);
        },
        afterTagRemoved: function(event, ui) {
            var tags = $("#big-search").tagit("assignedTags");
            if (!tags.length) {
                $('#big-search-container .tagit input').attr('placeholder', placeholder);
                $('#big-search-container button').removeClass('ready-to-search');
            } else {
                $('#big-search-container .tagit input').attr('placeholder', '');
                $('#big-search-container button').addClass('ready-to-search');
            }
        }
    });
    $('#big-search-container button').removeClass('ready-to-search');
    $('#big-search-container button').click(function(){
        sendSearchQuery();
    });

    $(document).on('keyup', '.tagit input', function(e){
        // on enter
        if (e.keyCode == 13 && !justAddedTag) {
            sendSearchQuery();
        }
    });

    function sendSearchQuery() {
        var tags = $("#big-search").tagit("assignedTags");
        if (tags.length) {
            window.location = 'http://' + window.location.hostname + '/search?query=' + tags.join(',');
        }
    }

    $(document).on('focus', '.tagit input', function(){
        $(this).closest('.tagit').addClass('active');
    });

    $(document).on('blur', '.tagit input', function(){
        $(this).closest('.tagit').removeClass('active');
    });

});