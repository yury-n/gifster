$(function(){

    var underlyingURL = null;

    _positionPrevNextButtons();

    function _positionPrevNextButtons() {
        var $resultsContainer = $('.results-container');
        if (!$resultsContainer.length) {
            return;
        }
        var fromLeft = $resultsContainer.offset().left;
        var fromRight = ($(window).width() - ($resultsContainer.offset().left + $resultsContainer.outerWidth(false)));

        $('#gallery-modal .btn-prev').css('left', fromLeft + 10);
        $('#gallery-modal .btn-next-container').css('right', fromRight + 10 - 16);
    }

    function renderGifModal(gifid) {

        var $modal = $('#gallery-modal');
        var gifData = window.gifs[gifid];
        $modal.find('.score-value').text(gifData['score'] > 0 ? '+' + gifData['score'] : gifData['score']);
        if (gifData['username']) {
            $modal.find('.from-user').show().attr('title', 'От пользователя ' + gifData['username']).find('.username').text(gifData['username']);
        } else {
            $modal.find('.from-user').hide();
        }
        var $h1 = $modal.find('h1');
        if (gifData['title'] != '') {
            $h1.text(gifData['title']).show();
        } else {
            $h1.hide();
        }
        var $gifContent = $modal.find('.gif-content');

        var $tags = $modal.find('.tags');
        $tags.html('');
        $.each(gifData['tags'], function(index, tag){
            var $a = $('<a>').attr('href', '/tag/' + tag['name'] + '/').text('#' + tag['name']);
            $tags.append($a);
        });
        $modal.find('.btn-share').removeClass('active');
        $modal.find('.share-area').removeClass('open');
        $modal.find('.gif').data('gifid', gifid);

        _sizeGifAndPositionModal($modal);
        renderGifContents($gifContent, gifid);
        updateGifPostWidth($modal.find('.gif-post'), gifid);

        var $upvote = $modal.find('.btn-upvote');
        if (gifData['vote'] == 1) {
            $upvote.addClass('active');
        } else {
            $upvote.removeClass('active');
        }
        var $downvote = $modal.find('.btn-downvote');
        if (gifData['vote'] == -1) {
            $downvote.addClass('active');
        } else {
            $downvote.removeClass('active');
        }
        var $fave = $modal.find('.btn-fave');
        if (gifData['is_faved']) {
            $fave.addClass('active');
        } else {
            $fave.removeClass('active');
        }

        var $thumb =_findOpenedThumb();
        var hasNext = $thumb.next('.thumb').length;
        var hasPrev = $thumb.prev('.thumb').length;

        if (hasNext) {
            $modal.removeClass('no-next');
        } else {
            //$modal.addClass('no-next');
        }

        if (hasPrev) {
            $modal.removeClass('no-prev');
        } else {
            $modal.addClass('no-prev');
        }

        if (!$thumb.closest('.results-container').length) {
            $modal.addClass('no-stream');
        } else {
            $modal.removeClass('no-stream');
        }

        _updateLocationToGifURL(gifid);

        renderShareButtons($modal.find('.share-btns'));

        $('#stats-iframe')[0].src = 'http://gifster.ru/stats/modal.html?s=' + Math.round(Math.random() * 1000);
    }


    function _sizeGifAndPositionModal($modal) {

        var gifid = $modal.find('.gif').data('gifid');

        var oldWidth = window.gifs[gifid]['width'];
        var oldHeight = window.gifs[gifid]['height'];
        var ratio = oldWidth / oldHeight;

        // to measure the modalHeight, will be returned to 'auto'
        $modal.find('.gif-content').html('').width(oldWidth).height(oldHeight);
        $modal.find('.gif-post').width(oldWidth);

        var minTopOffset = 120;
        var minBottomOffset = 100;

        var $modalDialog = $modal.find('.modal-dialog');
        var navbarHeight = 55;
        var dialogMarginTop = Math.round(($(window).height() - navbarHeight - $modalDialog.height()) / 2);
        var dialogMarginTop = Math.max(minTopOffset, navbarHeight + dialogMarginTop);
        $modalDialog.css('margin-top', dialogMarginTop);

        var dialogOffsetFromBottom = ($(window).height() - (dialogMarginTop - navbarHeight + $modalDialog.outerHeight(false)));
        if (dialogOffsetFromBottom < minBottomOffset && !window.isMobile) {
            var delta = Math.abs(minBottomOffset - dialogOffsetFromBottom);
            var newHeight = oldHeight - delta;
            var newWidth = Math.round(newHeight * ratio);
            window.gifs[gifid]['width'] = newWidth;
            window.gifs[gifid]['height'] = newHeight;
        }

        var delta = $(window).width() - $modalDialog.outerWidth(false);
        if (delta < 0) {
            var newWidth = oldWidth - Math.abs(delta);
            var newHeight = Math.round(newWidth / ratio);
            window.gifs[gifid]['width'] = newWidth;
            window.gifs[gifid]['height'] = newHeight;
        }

        $modal.find('.gif-content').width('auto').height('auto');
    }

    $(document).on('click', '.thumbnail', function(){
        var gifid = $(this).data('gifid');
        var $modal = $('#gallery-modal');
        $modal.show();
        // needed to capture module dimensions
        $modal.css('visibility', 'hidden');
        renderGifModal(gifid);
        $modal.css('visibility', 'visible');
        return false;
    });

    if (location.href.indexOf('ao=1') > -1) {
        $('.thumbnail:first').click();
    }

    $(document).on('contextmenu', '.gif-protector', function(e){

        var gifid = $(this).closest('.gif').data('gifid');

        var top = e.pageY;
        var left = e.pageX;
        $('#url-tooltip').css({'top': top, 'left': left}).show();
        $('#url-tooltip input').val(window.gifs[gifid]['original_gif']).focus().select();

        return false;
    });

    $(document).on('blur', '#url-tooltip input', function(e){
        $('#url-tooltip').hide();
    });

    $(document).on('click', '.modal .btn-next-container', function(){
        nextModal();
    });
    $(document).on('click', '.modal .btn-prev', function(){
        prevModal();
    });

    $(document).on('click', '.share-chat-input', function(){
        $(this).select();
    });

    $(document).on('click', '#gallery-modal', function(e){
        if (e.target == $('#gallery-modal')[0]) {
            terminate_gallery_modal();
        }
    });

    $(document).on('click', '#gallery-modal .btn-close', terminate_gallery_modal);
    $(document).on('click', '#gallery-modal .btn-cancel', terminate_gallery_modal);

    $(document).keydown(function(e) {
        if (e.keyCode == 39) {
            $('#gallery-modal .btn-next-container').addClass('active');
        } else if (e.keyCode == 37) {
            $('#gallery-modal .btn-prev').addClass('active');
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 39) { // right
            $('#gallery-modal .btn-next-container').removeClass('active');
            nextModal();
        } else if (e.keyCode == 37) { // left
            $('#gallery-modal .btn-prev').removeClass('active');
            prevModal();
        } else if (e.keyCode == 27) {
            terminate_gallery_modal();
        }
    });

    function terminate_gallery_modal() {
        updateLocationToUnderlyingURL();
    }

    function _updateLocationToGifURL(gifid) {
        if (underlyingURL == null) {
            underlyingURL = location.href;
        }
        var $thumbnail = $('.thumbnail[data-gifid="' + gifid + '"]')
        window.history.pushState({}, '', $thumbnail.attr('href'));
    }

    function updateLocationToUnderlyingURL() {
        window.history.pushState({}, '', underlyingURL);
        underlyingURL = null;
    }

    function nextModal() {

        var $thumb =_findOpenedThumb();
        var $nextThumb = $thumb.next('.thumb');
        if (!$nextThumb.length) {
            var fromGifPage = false;
            if (underlyingURL.indexOf('/gif/') > -1) {
                underlyingURL = 'http://' + window.location.hostname + '/';
                fromGifPage = true;
            }
            if (underlyingURL.indexOf('offset=') > -1) {
                var underlyingURLParts = underlyingURL.split('offset=');
                var urlBeforeOffset = underlyingURLParts[0];
                var currentOffset = parseInt(underlyingURLParts[1], 10);
                var redirectTo = urlBeforeOffset + 'ao=1&offset=' + (currentOffset + window.GIFS_PER_PAGE);
            } else {
                var redirectTo = underlyingURL + (underlyingURL.indexOf('?') > -1 ? '&' : '?') + 'ao=1&offset=' + (window.GIFS_PER_PAGE + (fromGifPage ? 1 : 0));
            }
            window.location = redirectTo;
            return;
        }
        var nextGifid = $nextThumb.find('.thumbnail').data('gifid');

        var $modal = $('#gallery-modal');
        $modal.find('.btn-next').removeClass('pulsating');

        renderGifModal(nextGifid);
        $modal[0].scrollTop = 0;

    }

    function prevModal() {

        var $thumb =_findOpenedThumb();
        var $prevThumb = $thumb.prev('.thumb');
        if (!$prevThumb.length) {
            return;
        }
        var prevGifid = $prevThumb.find('.thumbnail').data('gifid');

        var $modal = $('#gallery-modal');

        renderGifModal(prevGifid);
        $modal[0].scrollTop = 0;
    }

    function _findOpenedThumb() {
        var $gif = $('#gallery-modal .gif');
        var gifid = $gif.data('gifid');
        var $thumb = $('.thumbnail[data-gifid="' + gifid + '"]').closest('.thumb');
        return $thumb;
    }

});