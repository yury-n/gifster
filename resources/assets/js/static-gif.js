$(function(){

    $('.gif').each(function(){
        var gifid = $(this).data('gifid');
        if (!gifid) {
            return;
        }
        renderGifContents($(this).find('.gif-content'), gifid);
        updateGifPostWidth($(this).find('.gif-post'), gifid);
    });
});