var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass(
        [
            'app.scss', 'common.scss', 'navbar.scss', 'thumbnail.scss', 'gif.scss',
            'modal.scss', 'feedback-modal.scss', 'gallery-modal.scss', 'prev-next-btns.scss',
            'jumbotron.scss', 'entrance.scss', 'search.scss', 'gif-create.scss',
            'welcome-banner.scss', 'tag-it.scss'
        ],
        'public/css/app.css'
    );

    mix.scripts(
        [
            'common.js', 'feedback.js', 'gallery.js', 'modal.js', 'gif-create.js',
            'gif.js', 'referrer-killer.js', 'tag-it.js', 'search.js', 'static-gif.js'
        ],
        'public/js/app.js'
    );

    mix.version(['css/app.css', 'js/app.js']);

    mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/','public/build/fonts/bootstrap');
});
