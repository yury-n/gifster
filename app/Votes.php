<?php

namespace App;

use \DB;

class Votes {

    public static function get($userid, $gifids) {

        $query = DB::table('users_votes')
                   ->select('gifid', 'vote')
                   ->where('userid', '=', $userid)
                   ->whereIn('gifid', $gifids);

        return $query->get();
    }
}

?>
