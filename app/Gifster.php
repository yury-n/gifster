<?php

namespace App;

use \App\Helpers\Common;
use \DB;

class Gifster {

    const HOMEPAGE_SCORE_THRESHOLD = 0;

    public static function get_homepage_gifs($offset = 0, $limit = GIFS_PER_PAGE, $exclude_gifid = null) {

        $rows = DB::select('
            SELECT
                ' . Gif::get_fields() . ',
                users.name as username
            FROM
                gifs
            LEFT JOIN
                users ON gifs.userid = users.id
            WHERE
                gifs.score > ?
            ' . ($exclude_gifid ? ' AND gifs.id <> ' . intval($exclude_gifid) : '') . '
            ORDER BY
                id DESC
            LIMIT ?, ?
        ', [self::HOMEPAGE_SCORE_THRESHOLD, $offset, $limit + 1]);

        return self::process_gif_rows($rows, $limit + 1);
    }

    public static function get_faved_gifs($userid, $offset = 0, $limit = GIFS_PER_PAGE) {

        $rows = DB::select('
            SELECT
                ' . Gif::get_fields() . ',
                users.name as username
            FROM
                users_faves
            JOIN
                gifs ON users_faves.gifid = gifs.id
            LEFT JOIN
                users ON gifs.userid = users.id
            WHERE
                users_faves.userid = ?
            ORDER BY
                users_faves.id DESC
            LIMIT ?, ?
        ', [$userid, $offset, $limit + 1]);

        return self::process_gif_rows($rows, $limit + 1);
    }

    public static function get_newest_gifs($offset = 0, $limit = GIFS_PER_PAGE) {

        $rows = DB::select('
            SELECT
                ' . Gif::get_fields() . ',
                users.name as username
            FROM
                gifs
            LEFT JOIN
                users ON gifs.userid = users.id
            WHERE
                gifs.width > 0
            ORDER BY
                id DESC
            LIMIT ?, ?
        ', [$offset, $limit + 1]);

        return self::process_gif_rows($rows, $limit + 1);
    }

    public static function get_gifs_by_tagids($tagids, $offset = 0, $limit = GIFS_PER_PAGE, $and_or = 'or', $filter = null) {

        $rows = DB::select('
            SELECT
                ' . Gif::get_fields() . ',
                users.name as username
            FROM
                gifs
            LEFT JOIN
                users ON gifs.userid = users.id
            JOIN
                gifs_tags ON gifs.id = gifs_tags.gifid
            WHERE
                gifs_tags.tagid IN (' . implode(',', array_map('intval', $tagids)) . ') ' . '
                AND gifs.width > 0 ' .
            ($filter == 'relevant' ? 'AND gifs_tags.in_category = 1 ' : '') .
            ($filter == 'hot' ? 'AND gifs.score > ' . self::HOMEPAGE_SCORE_THRESHOLD . ' ' : '') .
            ($and_or == 'and' ? 'GROUP BY gifs.id HAVING COUNT(*) = ' . count($tagids) : '') . ' ' .
            'ORDER BY
                gifs.id DESC
            LIMIT ?, ?
        ', [$offset, $limit + 1]);

        return self::process_gif_rows($rows, $limit + 1);
    }

    public static function process_gif_rows($rows, $requested_num) {

        if (empty($rows)) {
            return [[], $has_more = false];
        }

        if ($requested_num == count($rows)) {
            $has_more = true;
            array_pop($rows);
        } else {
            $has_more = false;
        }

        $rows_by_id = [];
        foreach ($rows as $row) {
            $id = $row['id'];
            $rows_by_id[$id] = $row;
        }

        $gifids = array_keys($rows_by_id);

        $gifs_tags = Tags::get_gifs_tags($gifids);

        $faved_gifids = [];
        $votes_by_gifid = [];
        if (!\Auth::guest()) {
            $userid = \Auth::user()->id;
            $faved_gifids = Faves::get($userid, $gifids);
            $votes_by_gifid = array_pluck(Votes::get($userid, $gifids), 'vote', 'gifid');
        }

        foreach ($rows_by_id as $id => $row) {
            $gif_tags = $gifs_tags[$id];
            $rows_by_id[$id]['tags'] = $gif_tags;
            shuffle($gif_tags);
            $rows_by_id[$id]['shuffled_tags_str'] = implode(' ', array_column(array_slice($gif_tags, 0, 3), 'name'));
            $rows_by_id[$id]['random_tag'] = $gif_tags[array_rand($gif_tags)];
            $rows_by_id[$id]['is_faved'] = in_array($id, $faved_gifids);
            $rows_by_id[$id]['vote'] = isset($votes_by_gifid[$id]) ? $votes_by_gifid[$id] : 0;
        }

        foreach ($rows_by_id as $id => $row) {
            Gif::adjust_gif_data($rows_by_id[$id]);
        }

        return [$rows_by_id, $has_more];
    }
}
