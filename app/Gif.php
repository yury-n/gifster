<?php

namespace App;

use \App\Helpers\Common;
use \DB;

class Gif {

    public static function get_fields() {
        return 'gifs.id, gifs.userid, gifs.title, gifs.tech_type, gifs.score, gifs.original_gif, gifs.original_mp4, gifs.original_page, gifs.width, gifs.height, gifs.created_at, gifs.sourceid';
    }

    public static function get($gifid) {

        $gif_data = DB::select('
            SELECT
                ' . self::get_fields() . ',
                users.name as username
            FROM
                gifs
            LEFT JOIN
                users ON gifs.userid = users.id
            WHERE
                gifs.id = ?',
            [$gifid]);

        if (empty($gif_data)) {
            return [];
        }

        $gif_data = current($gif_data);

        $gif_data['tags'] = current(Tags::get_gifs_tags([$gifid]));
        $gif_data['shuffled_tags_str'] = implode(' ', array_column($gif_data['tags'], 'name'));
        $gif_data['random_tag'] = $gif_data['tags'][array_rand($gif_data['tags'])]['name'];

        self::adjust_gif_data($gif_data);

        if (!\Auth::guest()) {
            $userid = \Auth::user()->id;
            $gif_data['is_faved'] = self::is_faved($gifid, $userid);
            $gif_data['vote'] = self::get_vote($gifid, $userid);
        } else {
            $gif_data['is_faved'] = false;
        }
        $gif_data['title_trans'] = Common::translit($gif_data['title']);

        return $gif_data;
    }

    public static function get_thumb_url($gif_data) {

        $id = $gif_data['id'];

        return url('/imgs/uploads/thumbs/' . round($id / 1000) . '/' . $id . '.jpg');
    }

    public static function adjust_gif_data(&$gif_data) {
        self::adjust_dimensions($gif_data);
        $gif_data['thumb_url'] = self::get_thumb_url($gif_data);
        $gif_data['content'] = self::get_content($gif_data);
        if ($gif_data['tech_type'] == GIF_TECH_UPLOADED) {
            $gif_data['original_gif'] = \App\Helpers\Image::get_public_gif_url($gif_data['id']);
        }

        if (!empty($gif_data['title'])) {
            $title_trans = Common::translit(mb_strtolower($gif_data['title']));
        } else {
            $title_trans = Common::translit(implode(' ', array_column(array_slice($gif_data['tags'], 0, 3), 'name')));
        }
        $gif_data['translit'] = $title_trans;
    }

    public static function adjust_dimensions(&$gif_data) {

        if ($gif_data['width'] > $gif_data['height']) {
            $min_width = 600;
        } else {
            $min_width = 300;
        }
        $max_width = 700;

        $ratio = $gif_data['width'] / $gif_data['height'];

        if ($gif_data['width'] < $min_width) {
            $gif_data['width'] = $min_width;
            $gif_data['height'] = round($min_width / $ratio);
        } else if ($gif_data['width'] > $max_width) {
            $gif_data['width'] = $max_width;
            $gif_data['height'] = round($max_width / $ratio);
        }

    }

    public static function get_content($gif_data) {
        $id = $gif_data['id'];
        $sourceid = $gif_data['sourceid'];
        $tech_type = $gif_data['tech_type'];
        $original_gif = $gif_data['original_gif'];
        $original_mp4 = $gif_data['original_mp4'];

        $dimensions = ''; // now set in js

        if ($sourceid == GIF_SOURCE_GIPHY) {
            $tech_type = GIF_TECH_URL; // force it to use .gif cause it's sharper
        }

        if (\App\Helpers\Common::is_mobile()) {
            $tech_type = GIF_TECH_URL_VIDEO;
        }

        switch ($tech_type) {
            case GIF_TECH_UPLOADED:
                return '<img src="' . url('/imgs/uploads/gifs/' . round($id / 1000) . '/' . $id . '.gif') . '" ' . $dimensions .' />';
                break;
            case GIF_TECH_URL_VIDEO:
                return '<video src="' . $original_mp4 . '" autoplay loop ' . $dimensions . '></video>';
                break;
            case GIF_TECH_URL:
                return '<img src="' . $original_gif . '" ' . $dimensions . ' />';
            case GIF_TECH_EMPTY:
            default:
                return '';

        }
    }

    public static function create($data) {

        $id = \DB::table('gifs')->insertGetId([
            'tech_type' => $data['tech_type'],
            'userid' => $data['userid'],
            'title' => $data['title'],
            'original_gif' => $data['original_gif'],
            'original_mp4' => isset($data['original_mp4']) ? $data['original_mp4'] : '',
            'originalid' => isset($data['originalid']) ? $data['originalid'] : '',
            'is_mature' => $data['is_mature'],
            'score' => isset($data['score']) ? $data['score'] : 0,
            'sourceid' => isset($data['sourceid']) ? $data['sourceid'] : 0,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);

        return $id;
    }

    public static function set_gif_dimensions($gifid, $width, $height) {

        \DB::statement('UPDATE gifs SET width = ?, height = ? WHERE id = ? ', [$width, $height, $gifid]);
    }

    public static function fave($gifid, $by_userid) {

        \DB::statement('REPLACE INTO users_faves (userid, gifid) VALUES (?, ?)', [$by_userid, $gifid]);
    }

    public static function is_faved($gifid, $by_userid) {

        return (bool)count(Faves::get($by_userid, [$gifid]));
    }

    public static function get_vote($gifid, $by_userid) {

        $votes = Votes::get($by_userid, [$gifid]);

        if (empty($votes)) {
            return 0;
        }

        $vote = current($votes);

        if ($vote['vote'] > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    public static function unfave($gifid, $by_userid) {

        \DB::table('users_faves')->where('userid', '=', $by_userid)->where('gifid', '=', $gifid)->delete();
    }

    public static function vote($gifid, $by_userid, $vote) {

        \DB::statement('REPLACE INTO users_votes (userid, gifid, vote) VALUES (?, ?, ?)', [$by_userid, $gifid, $vote]);

        self::recalculate_score($gifid);
    }

    public static function recalculate_score($gifid) {

        \DB::statement('UPDATE gifs SET score = (SELECT SUM(vote) FROM users_votes WHERE gifid = ?) WHERE id = ? ', [$gifid, $gifid]);
    }
}
