<?php

namespace App\Helpers;

class Image
{

    /**
     * easy image resize function
     * @param  $file - file name to resize
     * @param  $width - new image width
     * @param  $height - new image height
     * @param  $proportional - keep image proportional, default is no
     * @param  $output - name of the new file (include path if needed)
     * @param  $delete_original - if true the original image will be deleted
     * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
     * @param  $quality - enter 1-100 (100 is best quality) default is 100
     * @return boolean|resource
     */
    public static function smart_resize_image($file,
                                $width = 0,
                                $height = 0,
                                $proportional = false,
                                $output = 'file',
                                $delete_original = true,
                                $use_linux_commands = false,
                                $quality = 100
    )
    {

        if ($height <= 0 && $width <= 0) return false;

        # Setting defaults and meta
        $info = getimagesize($file);
        $image = '';
        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;
        $cropHeight = $cropWidth = 0;

        # Calculating proportionality
        if ($proportional) {
            if ($width == 0) $factor = $height / $height_old;
            elseif ($height == 0) $factor = $width / $width_old;
            else                    $factor = min($width / $width_old, $height / $height_old);

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        } else {
            $final_width = ($width <= 0) ? $width_old : $width;
            $final_height = ($height <= 0) ? $height_old : $height;
            $widthX = $width_old / $width;
            $heightX = $height_old / $height;

            $x = min($widthX, $heightX);
            $cropWidth = ($width_old - $width * $x) / 2;
            $cropHeight = ($height_old - $height * $x) / 2;
        }

        # Loading image to memory according to type
        switch ($info[2]) {
            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($file);
                break;
            default:
                return false;
        }


        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image);
            $palletsize = imagecolorstotal($image);

            if ($transparency >= 0 && $transparency < $palletsize) {
                $transparent_color = imagecolorsforindex($image, $transparency);
                $transparency = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            } elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


        # Taking care of original, if needed
        if ($delete_original) {
            if ($use_linux_commands) exec('rm ' . $file);
            else @unlink($file);
        }

        # Preparing a method of providing result
        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = $file;
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        if ($quality < 100) {
            $info[2] = IMAGETYPE_JPEG;
        }

        # Writing image according to type to the output destination and image quality
        switch ($info[2]) {
            case IMAGETYPE_GIF:
                imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($image_resized, $output, $quality);
                break;
            case IMAGETYPE_PNG:
                $quality = 9 - (int)((0.9 * $quality) / 10.0);
                imagepng($image_resized, $output, $quality);
                break;
            default:
                return false;
        }

        return true;
    }

    public static function download_file($url, $path)
    {

        $newfilename = $path;
        $file = fopen($url, "rb");
        if ($file) {
            $newfile = fopen($newfilename, "wb");

            if ($newfile)
                while (!feof($file)) {
                    fwrite($newfile, fread($file, 1024 * 8), 1024 * 8);
                }
        }

        if ($file) {
            fclose($file);
        }
        if ($newfile) {
            fclose($newfile);
        }
    }

    public static function get_public_gif_url($gifid) {
        return 'http://' . $_SERVER['SERVER_NAME'] . '/imgs/uploads/gifs/' .  (round($gifid / 1000)) . '/' . $gifid . '.gif';
    }

    public static function get_public_thumb_url($gifid) {
        return 'http://' . $_SERVER['SERVER_NAME'] . '/imgs/uploads/thumbs/' .  (round($gifid / 1000)) . '/' . $gifid . '.jpg';
    }

    protected static function _get_gifs_folder() {
        return dirname(__FILE__) . '/../../public/imgs/uploads/gifs/';
    }

    protected static function _get_gifs_subfolder($gifid) {
        $gifs_folder = self::_get_gifs_folder();
        return $gifs_folder . (round($gifid / 1000)) . '/';
    }

    protected static function _get_thumbs_folder() {
        return dirname(__FILE__) . '/../../public/imgs/uploads/thumbs/';
    }

    protected static function _get_thumbs_subfolder($gifid) {
        $thumb_folder = self::_get_thumbs_folder();
        return $thumb_folder . (round($gifid / 1000)) . '/';
    }

    protected static function _create_upload_folders($gifid) {
        $gifs_folder = self::_get_gifs_folder();
        $gifs_subfolder = self::_get_gifs_subfolder($gifid);

        if (!file_exists($gifs_folder)) { mkdir($gifs_folder, 0755); }
        if (!file_exists($gifs_subfolder)) { mkdir($gifs_subfolder, 0755); }

        self::_create_thumb_folders($gifid);
    }

    protected static function _create_thumb_folders($gifid) {
        $thumb_folder = self::_get_thumbs_folder();
        $thumb_subfolder = self::_get_thumbs_subfolder($gifid);

        if (!file_exists($thumb_folder)) { mkdir($thumb_folder, 0755); }
        if (!file_exists($thumb_subfolder)) { mkdir($thumb_subfolder, 0755); }
    }

    public static function upload_from_url($gifid, $url, $ignore_mime = false) {

        self::_create_upload_folders($gifid);

        $gifs_subfolder = self::_get_gifs_subfolder($gifid);
        $thumb_subfolder = self::_get_thumbs_subfolder($gifid);

        $gif_uri = $gifs_subfolder . $gifid . '.gif';
        $thumb_uri = $thumb_subfolder . $gifid . '.gif';

        self::download_file($url, $gif_uri);

        if (!$ignore_mime && !self::is_valid_gif($gif_uri)) {
            unlink($gif_uri);
            return false;
        }
        list($width, $height) = getimagesize($gif_uri);
        rename($gif_uri, $thumb_uri);

        if (!self::create_thumb($thumb_uri)) {
            return false;
        }
        return ['width' => $width, 'height' => $height];
    }

    public static function upload_from_file($gifid, $file, $ignore_mime = false) {

        self::_create_upload_folders($gifid);

        $gifs_subfolder = self::_get_gifs_subfolder($gifid);
        $thumb_subfolder = self::_get_thumbs_subfolder($gifid);

        $gif_uri = $gifs_subfolder . $gifid . '.gif';
        $thumb_uri = $thumb_subfolder . $gifid . '.gif';

        if (isset($file['error'])
            && is_array($file['error'])) {
            return false;
        }

        if (move_uploaded_file($file['tmp_name'], $gif_uri)) {
            if (!$ignore_mime && !self::is_valid_gif($gif_uri)) {
                unlink($gif_uri);
                return false;
            }
            list($width, $height) = getimagesize($gif_uri);
            copy($gif_uri, $thumb_uri);
            if (!self::create_thumb($thumb_uri)) {
                return false;
            }
            return ['width' => $width, 'height' => $height];
        } else {
            return false;
        }
    }

    public static function upload_thumb_from_file($gifid, $file) {

        $thumb_subfolder = self::_get_thumbs_subfolder($gifid);
        $thumb_uri = $thumb_subfolder . $gifid . '.gif';

        if (move_uploaded_file($file['tmp_name'], $thumb_uri)) {
            list($width, $height) = getimagesize($thumb_uri);
            if (!self::create_thumb($thumb_uri)) {
                return false;
            }
            return ['width' => $width, 'height' => $height];
        } else {
            return false;
        }
    }

    public static function is_valid_gif($gif_uri) {

        $image_info = getimagesize($gif_uri);

        return $image_info['mime'] == 'image/gif';
    }

    public static function create_thumb($thumb_uri) {

        $thumb_uri_output = str_replace('.gif', '.jpg', $thumb_uri);

        return self::smart_resize_image($thumb_uri, 290, 200, false, $thumb_uri_output, true, false, 90);
    }

}

?>