<?php

namespace App;

use \DB;

class Faves {

    public static function get($userid, $gifids = [], $offset = 0, $limit = 20) {

        $query = DB::table('users_faves')
                   ->select('gifid')
                   ->where('userid', '=', $userid);

        if (!empty($gifids)) {
            $query->whereIn('gifid', $gifids);
        } else {
            $query->orderBy('gifid', 'desc');
            $query->skip($offset)->take($limit);
        }

        return array_column($query->get(), 'gifid');
    }
}

?>
