<?php

namespace App;

use \DB;

class Categories {

    protected static $_structure = [
        0 => [4497, 11554, 2260, 257, 525, 21, 21923, 497, 147],
        525 => [2828, 1599, 581, 5682, 1596, 168, 7335, 283, 2489]
    ];

    protected static $_currated_tagids = [
        // девушки
        147,
        // природа
        2828, 1599, 581, 5682, 1596, 168, 7335, 283, 2489
    ];

    protected static $_names = [
        147 => 'девушки',
        4497 => 'действия',
        11554 => 'веселое',
        2260 => 'эмоции',
        257 => 'животные',
        525 => 'природа',
        21 => 'спорт',
        21923 => 'реакции',
        497 => 'искусство',
        // природа
        2828 => 'облака',
        1599 => 'лес',
        581 => 'лава',
        5682 => 'северное сияние',
        1596 => 'океан',
        168 => 'снег',
        7335 => 'рассвет',
        283 => 'закат',
        2489 => 'водопад',
    ];

    protected static $_translit_names = [
        4497 => 'actions',
        11554 => 'funny',
        2260 => 'emotions',
        257 => 'animals',
        525 => 'nature',
        21 => 'sport',
        21923 => 'reactions',
        497 => 'art',
    ];

    public static function get_parent($tagid) {
        foreach (self::$_structure as $parent => $children) {
            if (in_array($tagid, $children)) {
                return $parent;
            }
        }
    }

    public static function is_tag_currated($tagid) {
        return in_array($tagid, self::$_currated_tagids);
    }

    public static function get_subtagids($tagid) {
        if (!isset(self::$_structure[$tagid])) {
            return [];
        } else {
            return self::$_structure[$tagid];
        }
    }

    public static function get_name($tagid) {
        return self::$_names[$tagid];
    }

    public static function get_tagid($name) {
        $flipped_names = array_flip(self::$_translit_names);
        return $flipped_names[$name];
    }

    public static function get_translit_name($tagid) {
        return self::$_translit_names[$tagid];
    }

    public static function get_thumb_gifids($tagids) {

        $rows = DB::table('categories')
                  ->select('tagid', 'thumb_gifid')
                  ->whereIn('tagid', $tagids)
                  ->get();

        if (empty($rows)) {
            return [];
        }

        return array_pluck($rows, 'thumb_gifid', 'tagid');
    }
}


?>
