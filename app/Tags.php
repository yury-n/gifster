<?php

namespace App;

use \DB;

class Tags {

    public static function get_gifs_tags($gifids) {

        $gifs_tags = DB::table('gifs_tags')
                         ->join('tags', 'gifs_tags.tagid', '=', 'tags.id')
                         ->select('gifs_tags.gifid', 'gifs_tags.tagid', 'tags.name')
                         ->whereIn('gifs_tags.gifid', $gifids)
                         ->get();

        $tags_by_gifid = [];
        foreach ($gifs_tags as $row) {
            $gifid = $row['gifid'];
            if (!isset($tags_by_gifid[$gifid])) {
                $tags_by_gifid[$gifid] = [];
            }
            $tags_by_gifid[$gifid][] = $row;
        }

        return $tags_by_gifid;
    }

    public static function sanitize_tags($tags) {
        $tags = array_map('trim', $tags);
        $tags = array_map('mb_strtolower', $tags);
        $tags = str_replace(['#'], '', $tags);
        $tags = str_replace(['ё'], 'е', $tags);
        $tags = array_unique($tags);

        return $tags;
    }

    public static function create_attach_tags($gifid, $tags) {

        $into_category_tagnames = [];
        foreach ($tags as & $tag) {
            if (strpos($tag, '!!!') !== false) {
                $tag = str_replace('!!!', '', $tag);
                $into_category_tagnames[] = $tag;
            }
        }

        $tags = self::sanitize_tags($tags);
        $tags = array_slice($tags, 0, 10);

        foreach ($tags as $key => $tag) {
            if (empty($tag)) {
                unset($tags[$key]);
            }
        }

        if (empty($tags)) {
            die('empty tags');
        }

        $rows = \DB::table('tags')->select('id', 'name')->whereIn('name', $tags)->get();
        $existing_tags = array_pluck($rows, 'name', 'id');

        $attach_tagids = array_keys($existing_tags);

        $missing_tags = [];
        foreach ($tags as $tag) {
            if (!in_array($tag, $existing_tags)) {
                $missing_tags[] = $tag;
            }
        }

        if (!empty($missing_tags)) {
            $insert_rows = array_map(function($tag){ return ['name' => $tag]; }, $missing_tags);
            \DB::table('tags')->insert($insert_rows);
            $last_tagid = \DB::getPdo()->lastInsertId();

            while (!empty($missing_tags)) {
                $attach_tagids[] = $last_tagid++;
                array_pop($missing_tags);
            }
        }

        $insert_rows = array_map(function($tagid) use ($gifid) { return ['tagid' => $tagid, 'gifid' => $gifid, 'in_category' => 0]; }, $attach_tagids);

        if (!empty($into_category_tagnames)) {
            foreach ($insert_rows as $key => $insert_row) {
                if (isset($existing_tags[$insert_row['tagid']])) {
                    $tagname = $existing_tags[$insert_row['tagid']];
                    if (in_array($tagname, $into_category_tagnames)) {
                        $insert_rows[$key]['in_category'] = 1;
                    }
                }
            }
            $set_gif_as_thumb_to_tagids = [];
            $existing_tags_flipped = array_flip($existing_tags);
            foreach ($into_category_tagnames as $tagname) {
                $into_category_tagid = $existing_tags_flipped[$tagname];
                // the tag iteself
                $set_gif_as_thumb_to_tagids[] = $into_category_tagid;
                // and all its parents
                $next_parent_tagid = $into_category_tagid;
                while ($next_parent_tagid = \App\Categories::get_parent($next_parent_tagid)) {
                    $set_gif_as_thumb_to_tagids[] = $next_parent_tagid;
                }
            }

            foreach ($set_gif_as_thumb_to_tagids as $tagid) {
                \DB::statement('REPLACE INTO categories (tagid, thumb_gifid) VALUES (?, ?)', [$tagid, $gifid]);
            }

        }

        \DB::table('gifs_tags')->insert($insert_rows);
    }

    public static function get_tagids($tags) {

        $rows = DB::table('tags')
                  ->select('tags.id')
                  ->whereIn('tags.name', $tags)
                  ->get();

        return array_column($rows, 'id');
    }

    public static function search_tags($query) {

        $rows = DB::table('tags')
                  ->select('tags.name')
                  ->where('tags.name', 'LIKE', $query . '%')
                  ->orderBy(DB::raw('length(tags.name)'))
                  ->take(5)
                  ->get();

        return array_column($rows, 'name');
    }
}
