<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use PHPHtmlParser\Dom;
use App\Helpers\Image;

class Admin extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function giphy_create(Request $request) {

        if (\Auth::user()->id != 1) {
            die('hello!');
        }

        $beta_key = 'dc6zaTOxFJmzC';

        $giphyid = $request['giphyid'];

        $gif_info = json_decode(file_get_contents('http://api.giphy.com/v1/gifs/' . $giphyid . '?api_key=' . $beta_key), true);
        $gif_info = $gif_info['data'];
        $gif_imgs = $gif_info['images'];
        $gif_original = $gif_imgs['original'];
        $gif_original_still = $gif_imgs['original_still'];

        $url = $gif_info['url'];
        $gif = $gif_original['url'];
        $gif_still = $gif_original_still['url'];
        $mp4 = $gif_original['mp4'];

        $tags = [];

        $dom = new Dom;
        $dom->loadFromFile($url);
        $dom_tags = $dom->find('.tag a');
        foreach ($dom_tags as $dom_tag) {
            $tags[] = $this->_get_text_translation($dom_tag->innerHtml);
        }

        return view('admin.giphy_create', compact('giphyid', 'gif', 'gif_still', 'mp4', 'tags'));
    }

    public function giphy_store(Request $request) {

        $gifid = \App\Gif::create([
            'tech_type' => GIF_TECH_URL_VIDEO,
            'userid' => 0,
            'title' => $request['title'],
            'original_mp4' => $request['mp4'],
            'original_gif' => $request['gif'],
            'originalid' => $request['giphyid'],
            'is_mature' => false,
            'score' => $request['score'],
            'sourceid' => GIF_SOURCE_GIPHY
        ]);

        \App\Tags::create_attach_tags($gifid, $request['tag']);

        $image_info = \App\Helpers\Image::upload_from_url($gifid, $request['gif_still']);

        \App\Gif::set_gif_dimensions($gifid, $image_info['width'], $image_info['height']);

        return redirect('/new');
    }

    public function pikabu_create_store(Request $request) {

        set_time_limit(0);
        ini_set('memory_limit','512M');
        ignore_user_abort(true);

        $is_gifv = true;

        $d = $request['d'];
        $page = $request['page'];

        if ($page == 10) {
            die('Done');
            die('<script>window.location = "http://" + window.location.hostname + "/admin_area/pikabu_create_store?d=' . ++$d . '&page=1";</script>');
        }

        if ($d == 2501) {
            die('Done');
        }

        $dom = new Dom;
        $dom->loadFromFile('http://pikabu.ru/search.php?st=2&d=' . $d . '&t=gif&page=' . $page);

        $dom_stories = $dom->find('.story');

        $rows = [];
        $pikabuids = [];

        $reached_negative_score = false;

        foreach ($dom_stories as $dom_story) {

            try {

                $pikabuid = $dom_story->getAttribute('data-story-id');
                $pikabuids[] = $pikabuid;

                $rating = intval($dom_story->find('.story__rating-count')->innerHtml);

                if ($rating < 0) {
                    $reached_negative_score = true;
                    break;
                }

                $dom_title = $dom_story->find('.story__header-title a');
                $title = html_entity_decode($dom_title->innerHtml);

                $original_thumb = $dom_story->find('.b-gifx__image')->getAttribute('src');

                $original_gif = str_replace('.jpg', '.gif', $original_thumb);
                $original_mp4 = $is_gifv ? str_replace(['.jpg', '.gif'], '.mp4', $original_thumb) : '';

                $dom_tags = $dom_story->find('.story__tag');
                $tags = [];
                foreach ($dom_tags as $dom_tag) {
                    $tag = mb_strtolower(trim($dom_tag->innerHtml));
                    if (mb_stripos($tag, '9гаг') !== false ||
                        mb_stripos($tag, '9gag') !== false ||
                        mb_stripos($tag, 'боян') !== false ||
                        mb_stripos($tag, 'баян') !== false ||
                        mb_stripos($tag, 'длиннопост') !== false ||
                        mb_stripos($tag, 'длинопост') !== false ||
                        mb_stripos($tag, 'gif') !== false ||
                        mb_stripos($tag, 'гиф') !== false) {

                        continue;
                    }
                    if (strcasecmp($tag, 'пикабу') == 0 ||
                        strcasecmp($tag, 'свежее') == 0 ||
                        strcasecmp($tag, 'горячее') == 0 ||
                        strcasecmp($tag, 'моё') == 0 ||
                        strcasecmp($tag, 'мое') == 0 ||
                        strcasecmp($tag, 'не мое') == 0 ||
                        strcasecmp($tag, 'не моё') == 0) {
                        continue;
                    }

                    $tags[$tag] = $tag;
                }

                if (empty($tags)) {
                    continue;
                }

            } catch (\Exception $e) {
                continue;
            }
            $rows[] = compact('pikabuid', 'title', 'tags', 'original_thumb', 'original_gif', 'original_mp4');
        }

        if (empty($rows)) {
            die('Done');
            die('<script>window.location = "http://" + window.location.hostname + "/admin_area/pikabu_create_store?d=' . ++$d . '&page=1";</script>');
        }

        $exiting_rows = \DB::table('gifs')->select('originalid')->whereIn('originalid', $pikabuids)->get();
        $existing_pikabuids = array_pluck($exiting_rows, 'originalid');

        foreach ($rows as $row) {

            if (in_array($row['pikabuid'], $existing_pikabuids)) {
                continue;
            }

            $gifid = \App\Gif::create([
                'tech_type' => ($is_gifv ? GIF_TECH_URL_VIDEO : GIF_TECH_URL),
                'userid' => 0,
                'title' => $row['title'],
                'original_mp4' => $row['original_mp4'],
                'original_gif' => $row['original_gif'],
                'originalid' => $row['pikabuid'],
                'is_mature' => false,
                'score' => 0
            ]);

            \App\Tags::create_attach_tags($gifid, $row['tags']);

            $image_info = \App\Helpers\Image::upload_from_url($gifid, $row['original_thumb'], true, $quality = 90);

            \App\Gif::set_gif_dimensions($gifid, $image_info['width'], $image_info['height']);

        }

        if ($reached_negative_score) {
            die('Done');
            die('<script>window.location = "http://" + window.location.hostname + "/admin_area/pikabu_create_store?d=' . ++$d . '&page=1";</script>');
        }

        die('<script>window.location = "http://" + window.location.hostname + "/admin_area/pikabu_create_store?d=' . $d . '&page=' . ++$page . '";</script>');
    }

    public function video_thumb(Request $request) {

        $url = $request['url'];

        return view('utils.video_thumb', compact('url'));
    }

    public function video_thumb_create(Request $request) {

        $url = $request['url'];
        $tags = [];

        return view('admin.video_thumb', compact('url', 'tags'));
    }

    public function video_thumb_store(Request $request) {

        $gifid = \App\Gif::create([
            'tech_type' => GIF_TECH_URL_VIDEO,
            'userid' => 0,
            'title' => $request['title'],
            'original_mp4' => $request['mp4'],
            'original_gif' => '',
            'originalid' => '',
            'is_mature' => intval($request['is_mature']),
            'score' => $request['score']
        ]);

        \App\Tags::create_attach_tags($gifid, $request['tag']);

        $image_info = Image::upload_thumb_from_file($gifid, $_FILES['gif-file']);

        \App\Gif::set_gif_dimensions($gifid, $image_info['width'], $image_info['height']);

        return redirect('/new');
    }

    protected function _get_text_translation($text, $direction = 'en-ru') {

        list($source, $target) = explode('-', $direction);

        $result = json_decode(file_get_contents('https://www.googleapis.com/language/translate/v2?key=AIzaSyB_TwO-Af2twTjG-2Wv66zvT1mYRdOS9sk&source=' . $source . '&target=' . $target . '&q=' . urlencode($text)), true);

        return $result['data']['translations'][0]['translatedText'];
    }
}

?>