<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Feedback extends Controller
{
    public function store(Request $request) {

        DB::table('feedback')->insert(
            [
                'email' => $request->input('email'),
                'message' => $request->input('msg'),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()')
            ]
        );
    }
}
