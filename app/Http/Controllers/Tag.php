<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\App;

class Tag extends Controller {

    public function search_tags(Request $request) {
        if (empty($request['query'])) {
            return [];
        }

        $tags = \App\Tags::search_tags($request['query']);

        if (empty($tags)) {
            return [];
        }

        $response = [];
        foreach ($tags as $tag) {
            $response[] = ['label' => $tag, 'value' => $tag];
        }

        echo json_encode($response);
    }

    public function get_gifs($tag, Request $request) {

        $offset = isset($request['offset']) ? $request['offset'] : 0;
        $next_offset = ($offset + GIFS_PER_PAGE);

        $tagids = \App\Tags::get_tagids([$tag]);
        if (!empty($tagids)) {
            list($gifs, $has_more) = \App\Gifster::get_gifs_by_tagids($tagids, $offset, GIFS_PER_PAGE, 'and', $request['filter']);
        } else {
            $gifs = [];
            $has_more = false;
        }

        return view('tag', compact('tag', 'gifs', 'next_offset', 'has_more'));
    }
}