<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Image;
use DB;

class Gifster extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth', ['only' => ['faves']]);
    }

    public function index(Request $request) {

        $navbar_section = 'index';

        $offset = isset($request['offset']) ? $request['offset'] : 0;
        $next_offset = ($offset + GIFS_PER_PAGE);

        list($gifs, $has_more) = \App\Gifster::get_homepage_gifs($offset, GIFS_PER_PAGE);

        $with_gifid = $request['with_gifid'];
        if ($with_gifid) {
            $gif = \App\Gif::get($with_gifid);
            $gifs = [$with_gifid => $gif] + $gifs;
            array_pop($gifs);
        }

        return view('index', compact('navbar_section', 'gifs', 'next_offset', 'has_more'));
    }

    public function newest(Request $request) {

        $navbar_section = 'new';
        $offset = isset($request['offset']) ? $request['offset'] : 0;
        $next_offset = ($offset + GIFS_PER_PAGE);

        list($gifs, $has_more) = \App\Gifster::get_newest_gifs($offset);

        return view('new', compact('navbar_section', 'gifs', 'next_offset', 'has_more'));
    }

    public function search(Request $request) {

        $navbar_section = 'search';
        $offset = isset($request['offset']) ? $request['offset'] : 0;
        $next_offset = ($offset + GIFS_PER_PAGE);

        $query = $request['query'];

        $tags = explode(',', $query);
        $tags = \App\Tags::sanitize_tags($tags);
        $tags = array_slice($tags, 0, 4);
        $tagids = \App\Tags::get_tagids($tags);

        if (count($tagids) == count($tags)) {
            list($gifs, $has_more) = \App\Gifster::get_gifs_by_tagids($tagids, $offset, GIFS_PER_PAGE, 'and');
        } else {
            $gifs = [];
            $has_more = false;
        }

        return view('search', compact('navbar_section', 'query', 'gifs', 'next_offset', 'has_more'));
    }

    public function faves(Request $request) {

        $navbar_section = 'faves';
        $offset = isset($request['offset']) ? $request['offset'] : 0;
        $next_offset = ($offset + GIFS_PER_PAGE);

        list($gifs, $has_more) = \App\Gifster::get_faved_gifs(\Auth::user()->id, $offset);

        return view('faves', compact('navbar_section', 'gifs', 'next_offset', 'has_more'));
    }

}
