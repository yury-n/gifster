<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use App\Helpers\Image;

class Gif extends Controller
{

    public function __construct() {
        $this->middleware('auth', ['except' => ['show', 'video_thumb']]);
    }

    public function create() {

        $navbar_section = 'gif/create';

        return view('gif.create', compact('navbar_section'));
    }

    public function store(Request $request) {

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $request['url_or_filename'] = trim($request['url_or_filename']);

        if (strpos($request['url_or_filename'], 'http://') === 0
            || strpos($request['url_or_filename'], 'https://') === 0) {
            $tech_type = GIF_TECH_URL;
        } else {
            $tech_type = GIF_TECH_UPLOADED;
        }

        $gifid = \App\Gif::create([
            'tech_type' => $tech_type,
            'userid' => \Auth::user()->id,
            'title' => $request['title'],
            'original_gif' => $tech_type == GIF_TECH_URL ? $request['url_or_filename'] : '',
            'is_mature' => (bool)$request['is_mature']
        ]);

        \App\Tags::create_attach_tags($gifid, explode(',', $request['tags']));

        if ($tech_type == GIF_TECH_URL) {
            $image_info = Image::upload_from_url($gifid, $request['url_or_filename']);
            if (!$image_info) {
                die('gif upload error!');
            }
        } else {
            $image_info = Image::upload_from_file($gifid, $_FILES['gif-file']);
            if (!$image_info) {
                die('gif upload error!');
            }
        }

        \App\Gif::set_gif_dimensions($gifid, $image_info['width'], $image_info['height']);

        return redirect('/new');
    }

    protected function validator(array $data) {

        $rules = [
            'tags' => 'required|max:256',
            'title' => 'max:256',
            'url_or_filename' => 'required',
        ];

        return Validator::make($data, $rules);
    }

    public function show($title_and_gifid) {

        $exploded = explode('_', $title_and_gifid);
        $gifid = end($exploded);

        if ($gifid == 35108) {
            return redirect()->intended('/?with_gifid=' . $gifid . '&ao=1');
        }

        $gif = \App\Gif::get($gifid);

        /*
        // gifs with similar tags
        $gif_tagids = array_column($gif['tags'], 'tagid');
        list($gifs) = \App\Gifster::get_gifs_by_tagids($gif_tagids);

        foreach ($gifs as $key => $one_of_gifs) {
            if ($one_of_gifs['id'] == $gifid) {
                unset($gifs[$key]);
            }
        }
        */

        list($gifs, $has_more) = \App\Gifster::get_homepage_gifs(0, GIFS_PER_PAGE, $gifid);
        $next_offset = GIFS_PER_PAGE;
        $next_offset_homepage = true;

        return view('gif.show', compact('gif', 'gifs', 'has_more', 'next_offset', 'next_offset_homepage'));
    }

    public function fave(Request $request) {

        \App\Gif::fave($request['gifid'], \Auth::user()->id);
    }

    public function unfave(Request $request) {

        \App\Gif::unfave($request['gifid'], \Auth::user()->id);
    }

    public function upvote(Request $request) {

        \App\Gif::vote($request['gifid'], \Auth::user()->id, 1);
    }

    public function downvote(Request $request) {

        \App\Gif::vote($request['gifid'], \Auth::user()->id, -1);
    }

}
