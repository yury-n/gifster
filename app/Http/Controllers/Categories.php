<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Image;
use DB;

class Categories extends Controller {

    public function index() {
        return $this->_show_category(0);
    }

    public function category($category) {

        $tagid = \App\Categories::get_tagid($category);

        return $this->_show_category($tagid);
    }

    protected function _show_category($tagid) {

        $navbar_section = 'categories';

        $subcategories = [];

        $subtagids = \App\Categories::get_subtagids($tagid);

        if (!empty($subtagids)) {

            $thumb_gifids = \App\Categories::get_thumb_gifids($subtagids);

            foreach ($subtagids as $subtagid) {

                $subtag_has_subtags = !empty(\App\Categories::get_subtagids($subtagid));

                if ($subtag_has_subtags) {
                    $category_or_tag_url = url('/category/' . \App\Categories::get_translit_name($subtagid) . '/');
                } else {
                    $is_subtag_currated = \App\Categories::is_tag_currated($subtagid);
                    $category_or_tag_url = url('/tag/' . \App\Categories::get_name($subtagid) . '/' . ($is_subtag_currated ? '?filter=relevant' : '?filter=hot'));
                }

                $subtag_name = \App\Categories::get_name($subtagid);
                $subcategories[$subtag_name] = [
                    'name' => $subtag_name,
                    'url' => $category_or_tag_url,
                    'thumb' => isset($thumb_gifids[$subtagid]) ? \App\Helpers\Image::get_public_thumb_url($thumb_gifids[$subtagid]) : ''
                ];
            }
        }

        ksort($subcategories);

        return view('categories', compact('subcategories', 'navbar_section'));
    }
}

?>
