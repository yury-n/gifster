<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', 'Gifster@index');
Route::get('/faves', 'Gifster@faves');
Route::get('/new', 'Gifster@newest');
Route::get('/search', 'Gifster@search');

Route::get('/category/{category}', 'Categories@category');
Route::get('/categories', 'Categories@index');

Route::get('/gif/create', 'Gif@create');
Route::get('/gif/{gifid}', 'Gif@show');

Route::post('/gif/store', 'Gif@store');
Route::post('/gif/fave', 'Gif@fave');
Route::post('/gif/unfave', 'Gif@unfave');
Route::post('/gif/upvote', 'Gif@upvote');
Route::post('/gif/downvote', 'Gif@downvote');

Route::get('/tag/{gifid}', 'Tag@get_gifs');
Route::get('/search_tags', 'Tag@search_tags');

Route::post('/feedback/store', 'Feedback@store');

Route::get('/admin_area/giphy_create', 'Admin@giphy_create');
Route::post('/admin_area/giphy_store', 'Admin@giphy_store');
Route::get('/admin_area/pikabu_create_store', 'Admin@pikabu_create_store');
Route::get('/admin_area/video_thumb', 'Admin@video_thumb');
Route::get('/admin_area/video_thumb_create', 'Admin@video_thumb_create');
Route::post('/admin_area/video_thumb_store', 'Admin@video_thumb_store');

// global contants for all requests

define('GIF_TECH_UPLOADED','1');
define('GIF_TECH_URL','2');
define('GIF_TECH_URL_VIDEO','3');
define('GIF_TECH_EMPTY','4');

define('GIF_SOURCE_GIPHY', 1);
define('GIF_SOURCE_PIKABU', 2);

define('GIFS_PER_PAGE', 48);