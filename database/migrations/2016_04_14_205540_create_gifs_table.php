<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 256)->default('');
            $table->tinyInteger('tech_type')->unsigned();
            $table->smallInteger('width')->unsigned();
            $table->smallInteger('height')->unsigned();
            $table->text('original_gif');
            $table->text('original_mp4')->default('');
            $table->text('original_page')->default('');
            $table->string('originalid', 64)->default('')->index();
            $table->boolean('is_mature')->default(false);
            $table->integer('userid')->unsigned();
            $table->smallInteger('score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gifs');
    }
}
