<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGifsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifs_tags', function (Blueprint $table) {
            $table->integer('gifid')->unsigned();
            $table->integer('tagid')->unsigned();
            $table->primary(['gifid', 'tagid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gifs_tags');
    }
}
