<?php

use Illuminate\Database\Seeder;

class GifsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gifs')->insert([
            [
                'tech_type' => GIF_TECH_URL,
                'userid' => 1,
                'title' => '',
                'original_gif' => 'http://orig00.deviantart.net/9bcd/f/2016/116/1/4/1_by_yury_n-da0c54z.gif',
                'originalid' => '',
                'width' => 400,
                'height' => 400,
                'score' => 10,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'tech_type' => GIF_TECH_URL,
                'userid' => 1,
                'title' => 'Когда навернулся на велике',
                'original_gif' => 'http://orig00.deviantart.net/2b70/f/2016/116/3/b/2_by_yury_n-da0c543.gif',
                'originalid' => '',
                'width' => 320,
                'height' => 343,
                'score' => 8,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'tech_type' => GIF_TECH_URL,
                'userid' => 1,
                'title' => '',
                'original_gif' => 'https://media.giphy.com/media/l70g43qOZL47C/giphy.gif',
                'originalid' => '',
                'width' => 498,
                'height' => 335,
                'score' => 4,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'tech_type' => GIF_TECH_URL,
                'userid' => 1,
                'title' => '',
                'original_gif' => 'https://media.giphy.com/media/zqXnds4QxHRZK/giphy.gif',
                'originalid' => '',
                'width' => 500,
                'height' => 206,
                'score' => 3,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ],
            [
                'tech_type' => GIF_TECH_URL_VIDEO,
                'userid' => 0,
                'title' => '',
                'original_gif' => 'http://media1.giphy.com/media/26vURuLUpPcWNsUnu/giphy.mp4',
                'originalid' => '26vURuLUpPcWNsUnu',
                'width' => 480,
                'height' => 480,
                'score' => 1,
                'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                'updated_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        ]);
    }
}
