<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'yury',
            'email' => 'yury@gmail.com',
            'password' => bcrypt('fella'),
        ]);
    }
}
