<?php

use Illuminate\Database\Seeder;

class GifsTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gifs_tags')->insert([
            ['gifid' => 1, 'tagid' => 1],
            ['gifid' => 1, 'tagid' => 2],
            ['gifid' => 1, 'tagid' => 3],

            ['gifid' => 2, 'tagid' => 2],

            ['gifid' => 3, 'tagid' => 7],
            ['gifid' => 3, 'tagid' => 1],

            ['gifid' => 4, 'tagid' => 8],
            ['gifid' => 4, 'tagid' => 9],
            ['gifid' => 4, 'tagid' => 10],

            ['gifid' => 5, 'tagid' => 11],
            ['gifid' => 5, 'tagid' => 12],
        ]);
    }
}
