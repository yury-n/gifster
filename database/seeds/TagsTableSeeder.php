<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            ['id' => '4497', 'name' => 'действия'],
            ['id' => '11554', 'name' => 'веселое'],
            ['id' => '2260', 'name' => 'эмоции'],
            ['id' => '257', 'name' => 'животные'],
            ['id' => '525', 'name' => 'природа'],
            ['id' => '21', 'name' => 'спорт'],
            ['id' => '21923', 'name' => 'реакции'],
            ['id' => '497', 'name' => 'искусство'],

            // природа
            ['id' => '2828', 'name' => 'облака'],
            ['id' => '1599', 'name' => 'лес'],
            ['id' => '581', 'name' => 'лава'],
            ['id' => '5682', 'name' => 'северное сияние'],
            ['id' => '1596', 'name' => 'океан'],
            ['id' => '168', 'name' => 'снег'],
            ['id' => '7335', 'name' => 'рассвет'],
            ['id' => '283', 'name' => 'закат'],
            ['id' => '2489', 'name' => 'водопад'],
        ]);
    }
}
